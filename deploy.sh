#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

REMOTE=$SMART_HOME_DOCKER_HOST_USER@$SMART_HOME_DOCKER_HOST # todo: change here

IMAGE=registry.lab.nexhome.ch/middleware/timeseries-bridge:latest # todo: change here

echo " * OPENING DOCKER SOCKET TUNNEL"
socat \
  "UNIX-LISTEN:/tmp/docker.sock,reuseaddr,fork" \
  "EXEC:'ssh -kTax $REMOTE socat STDIO UNIX-CONNECT\:/var/run/docker.sock'" \
  &

export DOCKER_HOST=unix:///tmp/docker.sock


echo " * LOGIN WITH GITLAB-CI TOKEN"
docker login -u $SMART_HOME_DOCKER_REGISTRY_USER -p $SMART_HOME_DOCKER_REGISTRY_PW registry.lab.nexhome.ch

echo " * PULLING NEW IMAGES"
docker pull $IMAGE

#echo " * CLEANING OLD IMAGES"
#ssh -t ${REMOTE} "docker-clean images"
