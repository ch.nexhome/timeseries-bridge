#!/usr/bin/env python3

import os
import re
import paho.mqtt.client as mqtt
from influxdb import InfluxDBClient

flux_host     = os.getenv("FLUXY_HOST", "localhost")
flux_port     = os.getenv("FLUXY_PORT", "8088")
flux_user     = os.getenv("FLUXY_USER", "admin")
flux_password = os.getenv("FLUXY_PASSWORD", "admin")
flux_database = os.getenv("FLUXY_DATABASE", "dummy")
mqtt_user     = os.getenv("MQTT_USER", "admin")
mqtt_password = os.getenv("MQTT_PASSWORD", "admin")
mqtt_host     = os.getenv("MQTT_HOST", "localhost")
mqtt_port     = os.getenv("MQTT_PORT", "1883")

fluxy = InfluxDBClient(flux_host, int(flux_port), flux_user, flux_password, flux_database)

def recast(payload):
    try:
        return float(payload)
    except:
        print("Unknown value type for '{}'".format(payload))

    return ret

def persist(data_group, thing_id, data_name, payload):
    measurement = "nexhome_{}".format(data_group)

    value = recast(payload)

    data = [{
            "measurement": measurement,
            "time_precision": "s",
            "tags": {
                "thing_id": thing_id
            },
            "fields": {
                data_name: value,
            }
        }]

    if not fluxy.write_points(data):
        raise Exception("Error writing metric to influxdb")

def mqtt_on_connect(client, userdata, flags, rc):
    print("Connected with result code {}".format(rc))
    client.subscribe("nexhome/data/#")

# Format defined in https://github.com/jorisbaiutti/BTI7252/wiki/02-Smart-Home-Commander-Api
# nexhome/data/38d61641-1475-452f-8ccd-a74ed59f31ca/temp
# nexhome/data/38d61641-1475-452f-8ccd-a74ed59f31ca/light
# nexhome/data/{thing_id}/{data_name}
#
def mqtt_on_message(client, userdata, msg):
    topic = re.match('nexhome/([^/]+)/([^/]+)/([^/]+)', msg.topic)

    if topic is None:
        print("Failed to parse topic {}".format(msg.topic))
        return

    data_group = topic.group(1)
    thing_id   = topic.group(2)
    data_name  = topic.group(3)

    print("thing_id={} data_name={} received".format(thing_id, data_name))

    persist(data_group, thing_id, data_name, msg.payload)

if __name__ == "__main__":
    mqtt_client = mqtt.Client()

    mqtt_client.username_pw_set(mqtt_user, mqtt_password)
    mqtt_client.on_connect = mqtt_on_connect
    mqtt_client.on_message = mqtt_on_message

    print("Server connecting")
    print("mqtt_port: {}".format(mqtt_port))
    mqtt_client.connect(mqtt_host, int(mqtt_port), 30)

    print("Server ready")
    mqtt_client.loop_forever()
